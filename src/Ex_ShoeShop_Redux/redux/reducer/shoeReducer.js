import { shoeArr } from "../../data";
import {
  BUY_SHOE,
  CHANGE_QUANTITY,
  DELETE_SHOE,
  VIEW_DETAIL,
} from "../constant/shoeConstant";

let inititalState = {
  shoeArr: shoeArr,
  detailShoe: shoeArr[0],
  cart: [],
};

export const shoeReducer = (state = inititalState, { type, payload }) => {
  switch (type) {
    case VIEW_DETAIL: {
      // payload: chứa object shoe
      state.detailShoe = payload;
      return { ...state };
    }
    case DELETE_SHOE: {
      let cloneCart = state.cart.filter((item) => {
        return item.id !== payload;
      });
      return { ...state, cart: cloneCart };
    }
    case BUY_SHOE: {
      // payload: chứa object là shoe
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id == payload.id);
      if (index == -1) {
        // Chưa có trong giỏ hàng
        // Copy shoe vào newShoe và thêm key soLUong
        let newShoe = { ...payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        // Có trong giỏ hàng
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }
    case CHANGE_QUANTITY: {
      // Payload: object chứa shoe và option
      let { shoe, option } = payload;
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id == shoe.id);
      cloneCart[index].soLuong += option;
      // Nếu số lượng =0 thì xóa
      if (cloneCart[index].soLuong == 0) {
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
