import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let { name, price, description, quantity } = this.props.detail;
    // console.log(`🚀 ~ DetailShoe ~ render ~ this.props`, this.props);
    return (
      <div>
        <h2>Detail</h2>
        <table class="table">
          <thead>
            <tr>
              <th>name</th>
              <th>price</th>
              <th>description</th>
              <th>quantity</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row">{name}</td>
              <td>{price}</td>
              <td>{description}</td>
              <td>{quantity}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detailShoe,
  };
};

export default connect(mapStateToProps)(DetailShoe);
